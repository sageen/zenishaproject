package com.zenisha.jsons.level1;

import java.util.HashMap;
import java.util.Map;

public class Dessert {
    private String ID;
    private String type;   
    
    
    public void setID(String ID)
	{
		this.ID= ID.trim();
	}
	
	public void setType(String type)
	{
		this.type = type.trim();
	}
	public String getID()
	{
		return this.ID;
	}
	
	public String getType()
	{
		return this.type;
	}
}
	
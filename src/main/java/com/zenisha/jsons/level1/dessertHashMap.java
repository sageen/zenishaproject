package com.zenisha.jsons.level1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class dessertHashMap {

	public static void main(String[] args) {

		String jsonString = "{ \"reg\": { \"id \": \"1001 \", \"type \": \"Regular \" }, \"cho\": { \"id \": \"1002 \", \"type \": \"Chocolate \" }, \"blu\": { \"id \": \"1003 \", \"type \": \"Blueberry \" }, \"dev\": { \"id \": \"1004 \", \"type \": \"Devil's Food \" } }";
		JSONObject jsonObject = new JSONObject(jsonString);
		List<Dessert> dessertList = getDessert(jsonString);
		Map<String, Map<String, String>> dessertData =getHashmapData(dessertList);
		printDessert(dessertData);
		
	}

	// fx1
	public static List<Dessert> getDessert(String jsonString) {

		JSONObject jsonObject = new JSONObject(jsonString);
		List<Dessert> dessertList = new ArrayList<Dessert>();

		Dessert reg = new Dessert();
		JSONObject regObject = jsonObject.getJSONObject("reg");
		reg.setID(regObject.getString("id "));
		reg.setType(regObject.getString("type "));
		dessertList.add(reg);

		Dessert cho = new Dessert();
		JSONObject choObject = jsonObject.getJSONObject("cho");
		cho.setID(choObject.getString("id "));
		cho.setType(choObject.getString("type "));
		dessertList.add(cho);

		Dessert blu = new Dessert();
		JSONObject bluObject = jsonObject.getJSONObject("blu");
		blu.setID(bluObject.getString("id "));
		blu.setType(bluObject.getString("type "));
		dessertList.add(blu);

		Dessert dev = new Dessert();
		JSONObject devObject = jsonObject.getJSONObject("dev");
		dev.setID((devObject.getString("id ")));
		dev.setType((devObject.getString("type ")));
		dessertList.add(dev);
		return dessertList;
	}
	 // fx2
	public static Map<String, Map<String, String>> getHashmapData(List<Dessert> dessertList){
		
			Dessert reg = dessertList.get(0);
			HashMap<String, String> regdata = new HashMap<String,String>();
			regdata.put("id ", reg.getID());
			regdata.put("type ", reg.getType());
			System.out.println(regdata);
			
			Dessert cho = dessertList.get(1);
			HashMap<String, String> chodata = new HashMap<String,String>();
			chodata.put("id ", cho.getID());
			chodata.put("type ", cho.getType());
			System.out.println(chodata);
			
			Dessert blu = dessertList.get(2);
			HashMap<String, String> bludata = new HashMap<String,String>();
			bludata.put("id ", blu.getID());
			bludata.put("type ", blu.getType());
			System.out.println(bludata);

			Dessert dev = dessertList.get(3);	
			HashMap<String, String> devdata = new HashMap<String,String>();
			devdata.put("id ", dev.getID());
			devdata.put("type ", dev.getType());
			System.out.println(devdata);
			
			Map<String, Map<String, String>> mainMap = new LinkedHashMap<String, Map<String,String>>();
			mainMap.put("reg", regdata);
			mainMap.put("cho", chodata);
			mainMap.put("blu", bludata);
			mainMap.put("dev", devdata);
			return mainMap;
			
	}
	// fx3
	public static void printDessert(Map<String, Map<String, String>> mainMap){
		System.out.println(mainMap);
	}
}

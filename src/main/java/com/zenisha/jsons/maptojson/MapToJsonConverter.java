package com.zenisha.jsons.maptojson;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


public class MapToJsonConverter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {

			ObjectMapper mapper = new ObjectMapper();
			String json = "";

			Map<String, String> a = new HashMap<String, String>();
			a.put("Emp Id", "001");
			a.put("Emp Name", "Ram");
			a.put("Emp Position", "Manager");
			a.put("Emp Company", "ABC");
			

			json = mapper.writeValueAsString(a);

			System.out.println(json);

			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(a);

			System.out.println(json);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
		

		
package com.zenisha.jsons.maptojson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

public class CakeHashMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Map<String, Object> data1Object = new LinkedHashMap<String, Object>();
		Map<String, Object> batters = new LinkedHashMap<String, Object>();
		data1Object.put("id", "0001");
		data1Object.put("type", "donut");
			
		Map<String, String> data1 = new HashMap<String, String>();
		data1.put("id ","1001");
		data1.put("type ","Regular");
		
		Map<String, String> data2 = new HashMap<String, String>();
		data2.put("id ","1002");
		data2.put("type ","Chocolate");
		
		Map<String, String> data3= new HashMap<String, String>();
		data3.put("id ","1003");
		data3.put("type ","Blueberry");
		
		Map<String, String> data4= new HashMap<String, String>();
		data4.put("id ","1004");
		data4.put("type ","Devil's Food");
		
		
		List<Map<String, String>> batter = new ArrayList<Map<String, String>>();
		batter.add(data1);
		batter.add(data2);
		batter.add(data3);
		batter.add(data4);
		
		batters.put("batter", batter);
		data1Object.put("batters", batters);
		
		String Donut=data1Object.toString();
		
		System.out.println(Donut);		
	}
}

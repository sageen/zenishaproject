package com.zenisha.jsons.parser;

import org.json.JSONArray;
import org.json.JSONObject;

public class CakeJsonParser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String jsonString = "{ \"id \": \"0001 \", \"type \": \"donut \", \"batters \": { \"batter \": [ { \"id \": \"1001 \", \"type \": \"Regular \" }, { \"id \": \"1002 \", \"type \": \"Chocolate \" }, { \"id \": \"1003 \", \"type \": \"Blueberry \" }, { \"id \": \"1004 \", \"type \": \"Devil's Food \" } ] } } ";
		JSONObject jsonObject= new JSONObject(jsonString);
		System.out.println("Json Object :: "+ jsonObject);
		
		String id;
		String type;
		JSONObject battersJson;
		
		id=jsonObject.getString("id ");
		type=jsonObject.getString("type ");
		battersJson = jsonObject.getJSONObject("batters ");
		JSONArray batterJson = battersJson.getJSONArray("batter ");
		
		System.out.println(id);
		System.out.println(type);

	}

}

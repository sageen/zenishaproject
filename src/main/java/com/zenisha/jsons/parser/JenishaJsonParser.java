package com.zenisha.jsons.parser;

import org.json.JSONArray;
import org.json.JSONObject;

public class JenishaJsonParser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// String jsonString =
 		String jsonString = "{\"balance\":1000.21,\"phonenum\":12345,\"is_vip\":true,\"name\":\"Jenisha\",\"address\":Basantapur,\"qualifiation\":[\"SLC\",\"+2\",\"BIM\"]}";
		JSONObject jsonObject= new JSONObject(jsonString);
		System.out.println("Json Object :: "+ jsonObject);
		
		double balance;
		int phone_Num;
		boolean isVip;
		String name;
		String address;
		JSONArray qualificationJson;
		String[] qualification = new String[3]; 
		
		balance = jsonObject.getDouble("balance");
		phone_Num = jsonObject.getInt("phonenum");
		isVip = jsonObject.getBoolean("is_vip");
		name = jsonObject.getString("name");
		address =jsonObject.getString("address");
		
		qualificationJson = jsonObject.getJSONArray("qualifiation");
		
		for(int i=0; i<qualificationJson.length();i++)
		{
			qualification[i] = qualificationJson.get(i).toString();
		}
		
		System.out.println("balance = "+balance);
		System.out.println("phoneNum = "+phone_Num);
		System.out.println("isVip = "+isVip);
		System.out.println("name = "+name);
		System.out.println("address = "+address);
		System.out.print("qualification = ");
		
		for(int i=0; i<qualification.length;i++)
		{
		System.out.print(qualification[i]+"\t");
		
		}
		
	}

}

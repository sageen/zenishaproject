package com.zenisha.jsons.parser;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonProfileParser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String jsonString = "{  \"id \":  \"0001 \",  \"type \":  \"donut \",  \"name \":  \"Cake \",  \"ppu \": 0.55,  \"batters \": {  \"batter \": [ {  \"id \":  \"1001 \",  \"type \":  \"Regular \" }, {  \"id \":  \"1002 \",  \"type \":  \"Chocolate \" }, {  \"id \":  \"1003 \",  \"type \":  \"Blueberry \" }, {  \"id \":  \"1004 \",  \"type \":  \"Devil's Food \" } ] },  \"topping \": [ {  \"id \":  \"5001 \",  \"type \":  \"None \" }, {  \"id \":  \"5002 \",  \"type \":  \"Glazed \" }, {  \"id \":  \"5005 \",  \"type \":  \"Sugar \" }, {  \"id \":  \"5007 \",  \"type \":  \"Powdered Sugar \" }, {  \"id \":  \"5006 \",  \"type \":  \"Chocolate with Sprinkles \" }, {  \"id \":  \"5003 \",  \"type \":  \"Chocolate \" }, {  \"id \":  \"5004 \",  \"type \":  \"Maple \" } ] }";
				JSONObject jsonObject= new JSONObject(jsonString);
		System.out.println("Json Object :: "+ jsonObject);
		
		String id;
		String name;
		JSONObject battersJson;
		
		id=jsonObject.getString("id ");
		name=jsonObject.getString("name ");
		battersJson = jsonObject.getJSONObject("batters ");
		JSONArray batterJson = battersJson.getJSONArray("batter ");
		
		System.out.println("id = "+id);
		System.out.println("name = "+name);
		System.out.println("batter= "+batterJson.get(1));	
	
		}
		
	}



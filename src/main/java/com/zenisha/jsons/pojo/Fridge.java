package com.zenisha.jsons.pojo;

public class Fridge {

	private String meat;
	private String drinks;

	
	public void setMeat(String meat)
	{
		this.meat= meat;
	}
	
	public void setDrinks(String drinks)
	{
		this.drinks = drinks;
	}
	
	public String getMeat()
	{
		return this.meat;
	}
	
	public String getDrinks()
	{
		return this.drinks;
	}
}

package com.zenisha.jsons.objecttojson;

import org.json.JSONArray;
import org.json.JSONObject;

public class CreateStringJson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CreateStringJson cjs = new CreateStringJson();
		cjs.createdJson();
	}
	public void createdJson() {
		JSONObject profileInfo = new JSONObject();
		profileInfo.put("id", "1002");
		profileInfo.put("name", "Cake");
		profileInfo.put("type", "Chocolate");
		
		JSONArray batters = new JSONArray();
		batters.put("Chocolate");
		
		profileInfo.put("batters", batters);
		
		System.out.println(profileInfo.toString());
	}

}

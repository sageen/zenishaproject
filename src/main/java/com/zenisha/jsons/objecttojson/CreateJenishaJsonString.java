package com.zenisha.jsons.objecttojson;

import org.json.JSONArray;
import org.json.JSONObject;

public class CreateJenishaJsonString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CreateJenishaJsonString cjs = new CreateJenishaJsonString();
		cjs.createJson();
	}

	public void createJson() {

		JSONObject mainInfo = new JSONObject();
		mainInfo.put("name", "Jenisha");
		mainInfo.put("Phone_num", 12345);
		mainInfo.put("balance", 1000.21);
		mainInfo.put("is_vip", true);
		mainInfo.put("address", "Basantapur");

		JSONArray qualification = new JSONArray();
		qualification.put("SLC");
		qualification.put("+2");
		qualification.put("BIM");

		mainInfo.put("qualifiation", qualification);

		System.out.println(mainInfo.toString());

	}

}

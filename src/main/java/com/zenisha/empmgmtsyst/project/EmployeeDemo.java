package com.zenisha.empmgmtsyst.project;

import java.sql.ResultSet;
import java.util.Scanner;

class EmployeeDemo {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		boolean y = true;
		do {
			System.out
					.println("Menu: \n1 Add \n2 Update \n3 Delete \n4 Search");
			Scanner sc = new Scanner(System.in);
			int menu = sc.nextInt();
			int choice;
			ResultSet rs;
			// Test t = new Test();
			EmpMgmtSystem obj = new EmpMgmtSystem();
			switch (menu) {
			case 1: {
				obj.add();

				break;
			}
			case 2: {
				obj.update();
				break;
			}
			case 3: {
				obj.delete();
				break;
			}
			case 4: {
				obj.searchAll();

				break;
			}
			default: {
				return;
			}
			}

			System.out.println("want to process more? y/n");
			String x = sc.next();
			char ch = x.charAt(0);
			if (ch == 'n') {
				y = false;
			}
		} while (y != false);

	}
}

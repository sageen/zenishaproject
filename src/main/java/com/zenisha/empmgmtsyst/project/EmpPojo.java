package com.zenisha.empmgmtsyst.project;

import java.util.Date;

public class EmpPojo {

	private long id;
	private String name;
	private String dob;
	private String post;
	private EducationPojo edu;

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDob() {
		return dob;
	}

	public String getPost() {
		return post;
	}

	
	public EducationPojo getEdu() {
		return edu;
	}

	public void setEdu(EducationPojo edu) {
		this.edu = edu;
	}

	@Override
	public String toString() {
		return "EmpPojo [id=" + id + ", name=" + name + ", dob=" + dob
				+ ", post=" + post + "]";
	}
}

package com.zenisha.empmgmtsyst.project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class EmpMgmtSystem {
	
	private String filePath="/home/zenisha/workspace/zenishaproject/emp.txt";

	static List<EmpPojo> EmployeeList = new ArrayList<EmpPojo>();
	Scanner sc = new Scanner(System.in);
	EmpPojo empPojo = new EmpPojo();

	public void add() throws JsonGenerationException, JsonMappingException,
			IOException {
		EducationPojo objEducation = new EducationPojo();
		boolean isValid = false;
		Date dob = null;
		long id = System.currentTimeMillis();
		System.out.println("Enter Employee Name: ");
		String name = sc.nextLine();// read input as String

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		SimpleDateFormat df = new SimpleDateFormat("dd/mm/yyyy");

		while (isValid == false) {
			try {
				System.out
						.println("Enter Employee D.O.B. in dd/mm/yyyy format:");
				String dateString = reader.readLine();
				dob = df.parse(dateString);
				isValid = true;

			} catch (java.text.ParseException e) {
				System.out.println("Please enter valid date!");
			}
		}

		System.out.println("Enter Employee Post");
		String post = sc.nextLine();// read input as String

		empPojo.setId(id);
		empPojo.setName(name);
		empPojo.setDob(dob.toString());
		empPojo.setPost(post);

		System.out.println("Institution Name: grade: section :");
		String institutionName = sc.nextLine();
		String grade = sc.nextLine();
		String section = sc.nextLine();

		boolean isValid1 = false;
		BufferedReader reader1 = new BufferedReader(new InputStreamReader(
				System.in));
		SimpleDateFormat df1 = new SimpleDateFormat("dd/mm/yyyy");
		Date from = null;
		while (isValid1 == false) {
			try {
				System.out.println("From");
				String dateString1 = reader1.readLine();

				from = df1.parse(dateString1);
				System.out.println(from.toString());
				isValid1 = true;

			} catch (java.text.ParseException e) {

				System.out.println("Please enter valid date!");
			}
		}
		boolean isValid2 = false;
		BufferedReader reader2 = new BufferedReader(new InputStreamReader(
				System.in));
		SimpleDateFormat df2 = new SimpleDateFormat("dd/mm/yyyy");
		Date to = null;
		while (isValid2 == false) {
			try {
				System.out.println("To:");
				String dateString2 = reader2.readLine();

				to = df2.parse(dateString2);
				System.out.println(to.toString());
				isValid2 = true;

			} catch (java.text.ParseException e) {
				System.out.println("Please enter valid date!");
			}
		}

		objEducation.setInstitutionName(institutionName);
		objEducation.setGrade(grade);
		objEducation.setSection(section);
		objEducation.setFrom(from.toString());
		objEducation.setTo(to.toString());
		empPojo.setEdu(objEducation);

		EmployeeList.add(empPojo);
		addToFile(empPojo);

	}

	public void update() throws IOException, ParseException,
			java.text.ParseException {
		EducationPojo updateEdu = new EducationPojo();
		EmpPojo emp = new EmpPojo();
		JSONParser parser = new JSONParser(); // string type lai jsonObject ma
												// lagey
		File fin = new File(filePath);// file
																				// ko
																				// object
																				// ban
																				// new
																				// file
																				// create
																				// gareko
		FileInputStream fis = new FileInputStream(fin);

		// Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String line = null;
		List<EmpPojo> allDatas = new ArrayList<EmpPojo>(); // EmpPojo ko list banako
		List<EmpPojo> allDatasCopy= new ArrayList<EmpPojo>(); //update garney record baheyk ko record add gardiney
		//ask user for data
		//allDatasCopy.add(emppojo);
		
		while ((line = br.readLine()) != null) {
			EducationPojo edu = new EducationPojo();
			System.out.println(line);
			JSONObject jsonObject = (JSONObject) new JSONParser().parse(line);// line
																				// parse
																				// gareko
			long id = (long) jsonObject.get("id");
			String name = (String) jsonObject.get("name");
			String dob1 = (String) jsonObject.get("dob");
			String post = (String) jsonObject.get("post");
			JSONObject innerJsonObject = (JSONObject) jsonObject.get("edu");

			edu.setInstitutionName(innerJsonObject.get("institutionName")
					.toString());
			edu.setGrade(innerJsonObject.get("grade").toString());
			edu.setSection(innerJsonObject.get("section").toString());
			edu.setFrom(innerJsonObject.get("from").toString());
			edu.setTo(innerJsonObject.get("to").toString());

			EmpPojo e = new EmpPojo(); // domain class banako id/name/dob/post
										// sabai huncha

			e.setId(id); // e ma values haru set gareko
			e.setName(name);
			e.setDob(dob1);
			e.setPost(post);
		

			e.setEdu(edu);

			allDatas.add(e);

		}

		br.close();

		Scanner sca = new Scanner(System.in);
		System.out.println("Enter the id to be updated:");// user lai delete
															// garnu parney id
															// mageyko
		long idToBeUpdated = sca.nextLong();

		System.out.println("Enter the updated name:");
		String name = sc.nextLine();// read input as String

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		SimpleDateFormat df = new SimpleDateFormat("dd/mm/yyyy");

		Date dob = null;
		boolean isValid = false; // code control garna use gareko
		while (isValid == false) {
			try {
				System.out
						.println("Enter Employee D.O.B. in dd/mm/yyyy format:");
				String dateString = reader.readLine();
				dob = df.parse(dateString);
				System.out.println(dob.toString());
				isValid = true; // correct output paye pachi loop break gardiney
			} catch (java.text.ParseException e) {

				System.out.println("Please enter valid date!");
			}
		}
		String post;
		System.out.println("Enter the updated post:");
		post = sc.nextLine();// read input as String

		String institutionName;
		System.out.println("Enter updated institution name:");
		institutionName = sc.nextLine();
		String grade;
		System.out.println("Enter updated grade:");
		grade = sc.nextLine();
		String section;
		System.out.println("Enter updated section:");
		section = sc.nextLine();

		BufferedReader read = new BufferedReader(new InputStreamReader(
				System.in));
		SimpleDateFormat df1 = new SimpleDateFormat("dd/mm/yyyy");

		Date from = null;
		// Date from;
		boolean isValidFrom = false; // code control garna use gareko
		while (isValidFrom == false) {
			try {
				System.out.println("Enter date to be updated (From dd/mm/yyyy):");
				String fromString = read.readLine();
				from = df1.parse(fromString);
				System.out.println(from.toString());
				isValidFrom = true; // correct output paye pachi loop break
									// gardiney
			} catch (java.text.ParseException e) {

				System.out.println("Please enter valid date!");
			}
		}
		BufferedReader reade = new BufferedReader(new InputStreamReader(
				System.in));
		SimpleDateFormat df2 = new SimpleDateFormat("dd/mm/yyyy");

		Date to = null;
		// Date to;
		boolean isValidTo = false; // code control garna use gareko
		while (isValidTo == false) {
			try {
				System.out.println("Enter date to be updated (To dd/mm/yyyy):");
				String toString = reade.readLine();
				to = df2.parse(toString);
				System.out.println(to.toString());
				isValidTo = true; // correct output paye pachi loop break
									// gardiney
			} catch (java.text.ParseException e) {
				System.out.println("Please enter valid date!");
			}
		}

		updateEdu.setFrom(from.toString());
		updateEdu.setTo(to.toString());
		updateEdu.setInstitutionName(institutionName);
		updateEdu.setGrade(grade);
		updateEdu.setSection(section);
		for (int i = 0; i < allDatas.size(); i++) {
			if (allDatas.get(i).getId() == idToBeUpdated) { // user le deko id
				// existing record ma
				// cha bhaye update
				// process ma janey
				allDatas.get(i).setDob(dob.toString()); // kunchai indexko i.e.
														// list.get(i), id
				// i.e. get(id)
				allDatas.get(i).setName(name);
				allDatas.get(i).setPost(post);
				allDatas.get(i).setEdu(updateEdu);

			}
			
		}
		
		for (int i = 0; i < allDatas.size(); i++) {
			if (allDatas.get(i).getId()!= idToBeUpdated) { // user le deko id
				// existing record ma
				// cha bhaye update
				// process ma janey
				EmpPojo object = new EmpPojo();
				object=allDatas.get(i);	
				
				allDatasCopy.add(object);

			}
			
		}
		
		
		replaceFile(allDatas); // purano record ma delete bhaisakey pachi ko
								// record
								// replace gardiney

	}

	public void delete() throws Exception {
		String line = null; //
		EducationPojo edu = new EducationPojo();
		JSONParser parser = new JSONParser(); // string type lai jsonObject ma
												// lagey
		File fin = new File(filePath);// file
																				// read
		FileInputStream fis = new FileInputStream(fin);

		// Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		List<EmpPojo> List = new ArrayList<EmpPojo>(); // EmpPojo ko list banako

		while ((line = br.readLine()) != null) {
			System.out.println(line);

			JSONObject jsonObject = (JSONObject) new JSONParser().parse(line);// line
																				// parse
																				// gareko

			long id = (long) jsonObject.get("id");
			String name = (String) jsonObject.get("name");
			String dob = (String) jsonObject.get("dob");

			String post = (String) jsonObject.get("post");

			JSONObject innerJsonObject = (JSONObject) jsonObject.get("edu");

			edu.setInstitutionName(innerJsonObject.get("institutionName")
					.toString());
			edu.setGrade(innerJsonObject.get("grade").toString());
			edu.setSection(innerJsonObject.get("section").toString());
			edu.setFrom(innerJsonObject.get("from").toString());
			edu.setTo(innerJsonObject.get("to").toString());

			EmpPojo e = new EmpPojo(); // domain class banako id/name/dob/post
										// sabai huncha

			e.setId(id); // e ma values haru set gareko
			e.setName(name);
			e.setDob(dob);
			e.setPost(post);

			e.setEdu(edu);

			List.add(e);

		}

		br.close();

		Scanner sca = new Scanner(System.in);
		System.out.println("Enter the id you want to delete:");// user lai
																// delete garnu
																// parney id
																// mageyko

		long idToBeDeleted = sca.nextLong();

		for (int i = 0; i < List.size(); i++) {
			if (List.get(i).getId() == idToBeDeleted) { // user le deko id
														// existing record ma
														// cha bhaye delete
														// gardiney
				List.remove(i);

			}
		}
		replaceFile(List); // purano record ma delete bhaisakey pachi ko record
							// replace gardiney

	}

	public void searchAll() throws ParseException, IOException,
			java.text.ParseException {

		
		JSONParser parser = new JSONParser(); // string type lai jsonObject ma
												// lagey
		File fin = new File(filePath);// file ko object ban new file create gareko
		FileInputStream fis = new FileInputStream(fin);

		// Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));

		String line;

		List<EmpPojo> List = new ArrayList<EmpPojo>(); // EmpPojo ko list banako

		while ((line = br.readLine()) != null) {

			JSONObject jsonObject = (JSONObject) new JSONParser().parse(line);// line
																				// parse
																				// gareko

			long id = (long) jsonObject.get("id");
			String name = (String) jsonObject.get("name");

			String dob1 = (String) jsonObject.get("dob");

			String post = (String) jsonObject.get("post");

			// reading inner object from json object
			JSONObject innerJsonObject = (JSONObject) jsonObject.get("edu");

			EducationPojo searchEdu = new EducationPojo();

			searchEdu.setInstitutionName(innerJsonObject.get("institutionName")
					.toString());
			searchEdu.setGrade(innerJsonObject.get("grade").toString());
			searchEdu.setSection(innerJsonObject.get("section").toString());
			searchEdu.setFrom(innerJsonObject.get("from").toString());
			searchEdu.setTo(innerJsonObject.get("to").toString());

			EmpPojo objEmpPojo = new EmpPojo(); // domain class banako
												// id/name/dob/post
			// sabai huncha

			objEmpPojo.setId(id); // e ma values haru set gareko
			objEmpPojo.setName(name);
			objEmpPojo.setDob(dob1);
			objEmpPojo.setPost(post);

			objEmpPojo.setEdu(searchEdu);

			List.add(objEmpPojo);

		}

		int i = 1;
		for (EmpPojo em : List) {
			System.out.println("*********** EMPLOYEE " + i + " **************");
			System.out.println("Id:" + em.getId());
			System.out.println("Name:" + em.getName());
			System.out.println("D.O.B:" + em.getDob());
			System.out.println("Post:" + em.getPost());
			EducationPojo educationPojoObject = em.getEdu();
			System.out.println("Institution Name:"
					+ educationPojoObject.getInstitutionName());
			System.out.println("Grade:" + educationPojoObject.getGrade());
			System.out.println("Section:" + educationPojoObject.getSection());
			System.out.println("From:" + educationPojoObject.getFrom());
			System.out.println("To:" + educationPojoObject.getTo());
			System.out.println("**************************************");
			System.out.println("--------------------------------------");
			i++;
		}
	}

	public void addToFile(EmpPojo em) throws JsonGenerationException,
			JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		String s = objectMapper.writeValueAsString(em);

		File file = new File("emp.txt");
		FileUtils.writeStringToFile(file, s + "\n", true);

	}

	public void replaceFile(List<EmpPojo> list) throws JsonGenerationException,
			FileNotFoundException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			int count = 1;
			File file = new File("emp.txt");
			for (EmpPojo e : list) {
				String s = objectMapper.writeValueAsString(e);
				if (count == 1) {
					FileUtils.writeStringToFile(file, s + "\n", false);
					count++;
				} else {
					FileUtils.writeStringToFile(file, s + "\n", true);

				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}

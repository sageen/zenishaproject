package com.zenisha.empmgmtsyst.project;

import java.util.Date;

public class EducationPojo {

	private String institutionName;
	private String grade;
	private String section;
	private String from;
	private String to;
	
	
	
	public String getInstitutionName() {
		return institutionName;
	}



	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}



	public String getGrade() {
		return grade;
	}



	public void setGrade(String grade) {
		this.grade = grade;
	}



	public String getSection() {
		return section;
	}



	public void setSection(String section) {
		this.section = section;
	}



	public String getFrom() {
		return from;
	}



	public void setFrom(String from) {
		this.from = from;
	}



	public String getTo() {
		return to;
	}



	public void setTo(String to) {
		this.to = to;
	}



	@Override
	public String toString() {
		return "EducationPojo [institutionName=" + institutionName + ", Grade=" + grade + ", section=" + section
				+ ", from=" + from + ", to=" + to +"]";
	}
}

